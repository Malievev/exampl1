import math
import numpy as np 
import glob
import os
import sys
import array

Type_noise = 0.1
Distance_noise = 0.1
Angle_noise = 0.1
Sensor_Distance=90
Sensor_Angle=100


class CarlaObject:

    def __init__(self, xPos, yPos, cone_type):
        self.xPos = xPos
        self.yPos = yPos
        self.type=cone_type
    def get_Type(self):
        return self.type
    def get_Position(self):
        Positiontuple = (self.xPos, self.yPos)
        return Positiontuple
    def get_X_Position(self):
        return self.xPos
    def get_Y_Position(self):
        return self.yPos



def calculate_distance(car_pose,cone_position):
    distance= math.hypot(car_pose[0]-cone_position[0], car_pose[1]-cone_position[1])
    return distance
def calculate_angle(car_pose,cone_position):
    x=cone_position[0] -car_pose[0]
    y=cone_position[1] -car_pose[1]
    rad_to_deg = lambda x: 180.0/math.pi * x
    cone_angle=math.atan2(y, x)
    angle_a=car_pose[2]-rad_to_deg(cone_angle)
    return angle_a


def noise_distance(Distance):
    # StadartNormalverteilung
    Erwartungswert=1
    Standartabweichung=math.sqrt(Distance_noise)
    Normalverteilung=(Distance-Erwartungswert)/ Standartabweichung
    return Normalverteilung
def noise_angle(Angle):
    Erwartungswert=1
    Standartabweichung=math.sqrt(Angle_noise)
    Normalverteilung=(Angle-Erwartungswert)/ Standartabweichung
    return Normalverteilung

def isInRange(Distance, car_pose):
    isEveryConevisible=False
    if Distance<=Sensor_Distance and (Angle<=(car_pose[2]+Sensor_Angle/2) or Angle<=(car_pose[2]-Sensor_Angle/2)):
        isEveryConevisible=True
    return isEveryConevisible
    
def simulate_sensor(CarlaObjeckts, car_pose):
    Sensor_measurements = list()
    for cone in CarlaObjeckts:
        Distance = calculate_distance(car_pose, cone.get_Position())
        isEveryConevisible=isINRange(Distance, car_pose)
        if isEveryConevisible:
            Angle = calculate_angle(car_pose, cone.get_Position())
            Cone_Type = cone.get_Type()
            Distance_noised = noise_distance(Distance)
            Angle_noised = noise_angle(Angle)
            Sensor_measurement =(Distance, Angle, Cone_Type) #(Distance_noised, Angle_noised, Type_noised)
            Sensor_measurements.append(Sensor_measurement)
        
        return Sensor_measurements



            
def main():
    K1 = CarlaObject(3,4,"green")
    K2 = CarlaObject(3,2,"blue")
    K3 = CarlaObject(400,22, "orange")
    car_pose = [0, 0, 90]
    CarlaObjeckts=[K1,K2]
    CarlaObjeckts.append(K3)
  
    Measurments=simulate_sensor(CarlaObjeckts, car_pose)
    print(Measurments)
    
if __name__ == '__main__':
    main()