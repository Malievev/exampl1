#!/usr/bin/env python

import os
import csv
import math
import rospy
import numpy as np
from nav_msgs.msg import Odometry
import tf
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from location.msg import Cone
from location.msg import Perception
import random
import time

import argparse
import json

import carla

Type_noise = 0.1
Distance_noise = 0.1
Angle_noise = 0.1
Sensor_Distance=7
Sensor_Angle=200

class SensorSimulate(object):

    def __init__(self):

        # 1.2 Initialize ROS node
        rospy.init_node('SensorSimulate')

        # define state variables
        self.position = None
        self.carpostion_x = None
        self.carpostion_y = None
        self.v = None
        self.yaw = None
        self.car_angle=None
        self.start_time = None
        self.previous_time = None
        self.waypoints = None
        self.CarlaObjeckts=None
        self.publisher = None
        self.cone_list = []
        # wait ROS master is ready
        self.wait_node_initialization()
        #  Subscribe to topic
        rospy.Subscriber('/carla/ego_vehicle/odometry', Odometry, self.callback)
        self.publisher = rospy.Publisher('/detection', Perception, queue_size=2)
        self.host = rospy.get_param('/carla/host', '127.0.0.1')
        #self.host = rospy.get_param('/carla/host', '10.0.2.2')
        self.port = rospy.get_param('/carla/port', '2000')
        self.timeout = rospy.get_param('/carla/timeout', '2')
        client = carla.Client(self.host, self.port)
        client.set_timeout(2.0)

        world = client.get_world()
        a = world.get_actors()
        Yellowcone="coneYellow"
        conesyellow = a.filter("static.prop.coneyellow")
        if len(conesyellow)>0:
            for cones in range(len(conesyellow)):
                location = conesyellow[cones].get_location()
                x=location.x
                y=location.y
                coneyellowlist=(Yellowcone, x, y)
                self.cone_list.append(coneyellowlist)

            #print("{}".format(coneyellow))
        Bluescone="coneBlue"
        conesblue = a.filter("static.prop.coneblue")
        if len(conesblue)>0:
            for cones in range(len(conesblue)):
                location = conesblue[cones].get_location()
                x=location.x
                y=location.y
                coneblielist=(Bluescone, x, y)
                self.cone_list.append(coneblielist)
        Orangecone="coneOrange"
        conesorange = a.filter("static.prop.coneorange")
        if len(conesorange)>0:
            for cones in range(len(conesorange)):
                location = conesorange[cones].get_location()
                x=location.x
                y=location.y
                coneorangelist=(Orangecone, x, y)
                self.cone_list.append(coneblielist)
        OrangeconeBig="coneOrangeBig"
        conesorangebig = a.filter("static.prop.coneorangebig")
        if len(conesorangebig)>0:
            for cones in range(len(conesorangebig)):
                location = conesorangebig[cones].get_location()
                x=location.x
                y=location.y
                coneorangebiglist=(OrangeconeBig, x, y)
                self.cone_list.append(coneorangebiglist)
        #self.cone_list.append( (coneyellowlist, coneblielist, coneorangelist, coneorangebiglist) )


        # subscribe to Carla odometry to get current position with self.process_position callback
        #rospy.Subscriber('/carla/ego_vehicle/odometry', Odometry, self.get_coneobjectlist)
        self.wait_initialization()
        # run node loop
        self.loop()





    def callback(self, position):
        self.position = position.pose

        self.carpostion_x = self.position.pose.position.x
        self.carpostion_x= round(self.carpostion_x, 2)
        self.carpostion_y = self.position.pose.position.y
        self.carpostion_y=self.carpostion_y
        self.carpostion_y=round(self.carpostion_y, 2)
        #rospy.loginfo('Carposition: x: {}, y: {}'.format(self.carpostion_x, self.carpostion_y))
        self.yaw = self.get_yaw_last_position()
        self.yaw = round(self.yaw, 2)
        rad_to_deg = lambda x: 180.0/math.pi * x * -1
        self.car_angle=rad_to_deg(self.yaw)
        self.car_angle=round(self.car_angle, 2)

        #rospy.loginfo('angle: {}'.format(self.car_angle))


    # method to convert quaternions to yaw angle
    def get_yaw_last_position(self):
        quaternion = (
                        self.position.pose.orientation.x,
                        self.position.pose.orientation.y,
                        self.position.pose.orientation.z,
                        self.position.pose.orientation.w)

        euler = tf.transformations.euler_from_quaternion(quaternion)
        yaw = euler[2]

        return yaw


    def wait_node_initialization(self):
        rate = rospy.Rate(10)

        if not rospy.is_shutdown():
            rospy.loginfo('ROS initialized.')


    def wait_initialization(self):
        rate = rospy.Rate(10)

        # wait till we get position initialized
        while not rospy.is_shutdown() and not self.position:
            rate.sleep()

        if not rospy.is_shutdown():
            rospy.loginfo('Connected to vehicle')


    def loop(self):
        rate = rospy.Rate(10)
        #message=Perception()
        msg_measurment = Cone()
        #rospy.loginfo('Carposition: x: {}, y: {}'.format(self.carpostion_x, self.carpostion_y))

        while not rospy.is_shutdown():
            Sensor_measurements = list()
            for cone in (self.cone_list):
                #print(cone)
                Distance = self.calculate_distance(cone)#(self.cone_list[cone]))
                Distance_to_car = self.calculate_distance_to_car(cone)
                Distance_to_cones = self.calculate_distance_to_cones(cone)
                Angle = self.calculate_angle(cone)#self.cone_list[cone])
                #rospy.loginfo('YYYYYYYYYYYY: {}, Angle_care: {}, Distance: {}'.format(cone[0], Distance, Angle))
		#print('distance', Distance)
                #print('angle', Angle)
                if Distance<=Sensor_Distance: #and Distance_to_cones<=Distance_to_car:
                    if Angle<=(Sensor_Angle/2):# or Angle<=(self.car_angle-Sensor_Angle/2)): # Check if isEveryConevisible:
                         Cone_Type = cone[0]
                         Distance_noised = self.noise_distance(Distance)
                         Angle_noised = self.noise_angle(Angle)
                         if Cone_Type=="coneYellow":
                             Cone_Type="y"
                         elif Cone_Type=="coneBlue":
                             Cone_Type="b"
                         elif Cone_Type=="coneOrange":
                             Cone_Type="o"
                         elif Cone_Type=="coneOrangeBig":
                             Cone_Type="ob"
                         msg_measurment = Cone()
                         message = Perception()
                         msg_measurment.header.frame_id = "sensor_frame"
                         msg_measurment.header.stamp = rospy.get_rostime()
                         msg_measurment.xpos=self.carpostion_x
                         msg_measurment.ypos=(1)*self.carpostion_y
                         msg_measurment.heading=self.yaw
                         msg_measurment.color=Cone_Type
                         msg_measurment.distance=Distance_noised
                         msg_measurment.angle=Angle_noised
                         message.Cone_detections += [msg_measurment]
                         message.header.frame_id = "sensor_frame"
                         message.header.stamp = rospy.get_rostime()
                         rospy.loginfo('Cones: {}, x: {}, y: {}, car_x: {}, car_y: {}, headinginrad: {}, headingindegree: {}'.format(Cone_Type, cone[1], cone[2], self.carpostion_x, self.carpostion_y, self.yaw, self.car_angle))
                         self.cone_list.remove(cone)
			 self.publisher.publish(message)
			 del message#.clear()


        #self.publisher.publish(message)
        #rate.sleep()

        return None#Sensor_measurements


    def calculate_distance(self, cone_position):
        distance= math.hypot(self.carpostion_x-cone_position[1], ((1)*self.carpostion_y)-((-1)*cone_position[2]))
        #distance_to_car= math.hypot(self.carpostion_x, ((-1)*self.carpostion_y))
        #distance_to_cones= math.hypot(cone_position[1], cone_position[2])
        #rospy.loginfo('Distancex: {} '.format(distance))
        return distance

    def calculate_distance_to_car(self, cone_position):
        distance_to_car= math.hypot(self.carpostion_x, ((1)*self.carpostion_y))
        #rospy.loginfo('Distancex: {} '.format(distance))
        return distance_to_car

    def calculate_distance_to_cones(self, cone_position):
        distance_to_cones= math.hypot(cone_position[1], cone_position[2])
        #rospy.loginfo('Distancex: {} '.format(distance))
        return distance_to_cones
    def calculate_angle(self, cone_position):
        #x=cone_position[1]
        #y=cone_position[2]
        #xx=self.carpostion_x
        #yy=(-1)*self.carpostion_y

        delta_x_pos= cone_position[1] -self.carpostion_x
        delta_y_pos=  ((-1)*cone_position[2]) -((1)*self.carpostion_y)
        #delta_x_pos=abs(delta_x_pos)
        #delta_y_pos=abs(delta_y_pos)
        #rad_to_deg = lambda x: 180.0/math.pi * x
        #cone_angle=math.atan2(y, x)
        #angle_car=math.atan2(yy, xx)
        angle=math.atan2(delta_y_pos, delta_x_pos)
        #print('angle')
        #print(angle)
        #cone_angle=rad_to_deg(cone_angle)
        #angle_car=rad_to_deg(angle_car)
        #angle_a=self.car_angle-rad_to_deg(cone_angle)
        rad=1.5708
        #degree=90
        #print(self.yaw)
        #print(rad)
        #print('heading', self.yaw)
        small_angle=self.yaw
        #small_angle=degree -self.car_angle
        #angle=rad_to_deg(angle)
        angle_a=angle +small_angle
        #print('angle_a', angle_a)

        #rospy.loginfo('Cone: {}, Angle: {}'.format(cone_position, angle_difference))
        return angle_a


    def noise_distance(self,Distance):
        Erwartungswert=Distance
        Varianz=1
        #verteilung=1 / math.sqrt((2 * math.pi*Varianz)**2) * math.exp(-0.5 * ((Distance-Erwartungswert)**2)*Varianz)
        verteilung = np.random.normal(Erwartungswert, Varianz)

        return verteilung
    def noise_angle(self, Angle):
        Erwartungswert=Angle
        Varianz=0.02
        #verteilung=1 / math.sqrt((2 * math.pi*Varianz)**2) * math.exp(-0.5 * ((Angle-Erwartungswert)**2)*Varianz)
        verteilung = np.random.normal(Erwartungswert, Varianz)
        return verteilung

    def isInRange(self, Distance, Angle):
        isEveryConevisible=False
        if Distance<=Sensor_Distance and (Angle<=(self.car_angle+Sensor_Angle/2) or Angle<=(self.car_angle-Sensor_Angle/2)):
            isEveryConevisible=True
        return isEveryConevisible #isEveryConevisible





if __name__ == '__main__':
    try:
        SensorSimulate()
    except rospy.ROSInterruptException:
        rospy.logerr('Could not start mission planner node.')
        pass
